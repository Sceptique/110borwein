import sys
import math

def output(r, m):
    print(str(m) + "    \t:" + str(r) + " soit une difference de " + str((math.pi / 2 - r)))

def calc(n, x):
    r = 1
    k = 0
    if x == 0:
        return r
    while k <= n:
      r = r * (math.sin(x / (2*k + 1)) / (x / (2*k + 1)))
      k += 1
    return r

def rectangle(n, h):
    r = 0
    i = 0
    while i < 10000:
        r = r + calc(n, i * h)
        i += 1

    r = r * h
    r = abs(r)
    output(r, "rectangle")

def trapeze(n, h):
    r = 0
    i = 1
    while i < 10000:
      r = r + calc(n, i * h)
      i += 1

    r = ((r * 2 + calc(n, 0) + calc(n, 5000)) * (5000 - 0) / (DIVISION * 2))
    r = abs(r)
    output(r, "trapeze")

def simpson(n, h):
    r1 = 0
    r2 = 0

    i = 1
    while i < 10000:
      r1 = r1 + calc(n, 0 + i * h)
      i += 1

    i = 0
    while i < 10000:
      r2 = r2 + calc(n, 0 + i * h + h / 2)
      i += 1

    r = (r1 * 2 + r2 * 4 + calc(n, 0) + calc(n, 5000)) * (5000 - 0) / (DIVISION * (4 + 2))
    r = abs(r)
    output(r, "simpson")

DIVISION = 10000
h = (0 - 5000) / DIVISION

num_argv = 1
if len(sys.argv) < 2:
    print("Erreur : préciser n")
    exit(-1)

if sys.argv[1] == '--python':
    num_argv = 2
    if len(sys.argv) < 3:
        print("Erreur : préciser n")
        exit(-1)
if (int(sys.argv[num_argv]) < 0):
    print("Erreur : n doit etre forcement superieur ou egale a 0")
    exit(-1)

n = 0
try:
    n = int(sys.argv[num_argv])
except:
    print("Erreur : n n'est pas valide")
    exit(-1)

rectangle(n, h)
trapeze(n, h)
simpson(n, h)

exit(0)
