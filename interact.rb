#!/usr/bin/env ruby
#encoding: utf-8

def interactif options
  if options[:n] == false
    options[:n] = 0
  end

  trap("SIGINT"){print "\n" + options[:prompt]}
  loop do
    if options[:infos] == true
      puts "n:\t\t#{options[:n]}\txmax:\t#{options[:xmax]}\tdivisions:\t#{options[:divisions].to_i.to_s}\tprecision:\t#{options[:precision]}"
      puts "display:\t#{options[:display]}\toutput:\t#{options[:output]}\tresolution:\t#{options[:resolution]}\ttitle:\t#{options[:title]}"
    end
    print options[:prompt]
    cmd = $stdin.gets.to_s.chomp
    if (cmd == "")
      puts
      next
    end
    cmds = cmd.split

    if cmds[0].match /\A(n)\Z/
      options[:n] = cmds[1].to_i

    elsif cmds[0].match /\A(c(alc)?)\Z/
      calculate options

    elsif cmds[0].match /\A(display)\Z/
      options[:display] = true

    elsif cmds[0].match /\A(d(ivisions)?)\Z/
      options[:divisions] = cmds[1].to_f

    elsif cmds[0].match /\A(exit)|(quit)\Z/
      return true

    elsif cmds[0].match /\A(gauss)|(rectangle)|(simpson)|(trapeze)\Z/
      options[:methods] = [cmds[0]]
      calculate options

    elsif cmds[0].match /\A(h(elp)?)\Z/
      puts "Available commands :"
      puts "\tc, calc"
      puts "\tdisplay"
      puts "\td, divisions"
      puts "\texit"
      puts "\tgauss"
      puts "\th, help"
      puts "\tinfo"
      puts "\tm, man"
      puts "\tn"
      puts "\tnodisplay"
      puts "\tnoinfo"
      puts "\to, out, output"
      puts "\tp, precision"
      puts "\tprompt"
      puts "\tquit"
      puts "\trectangle"
      puts "\tr, resolution"
      puts "\tsimpson"
      puts "\tt, title"
      puts "\ttrapeze"
      puts "\txmax"

    elsif cmds[0].match /\A(info)\Z/
      options[:infos] = true

    elsif cmds[0].match /\A(m(an)?)\Z/
      exec("man ./110borwein.man")

    elsif cmds[0].match /\A(nodisplay)\Z/
      options[:display] = false

    elsif cmds[0].match /\A(noinfo)\Z/
      options[:infos] = false

    elsif cmds[0].match /\A(o(ut(put)?)?)\Z/ and cmds[i].match /(jpg)|(png)|(pdf)/
      options[:output] = cmds[i].to_s

    elsif cmds[0].match /\A(p(recision)?)\Z/
      options[:precision] = cmds[1].to_i

    elsif cmds[0].match /\A(prompt)\Z/
      options[:prompt] = cmds[i].to_s

    elsif cmds[0].match /\A(r(esolution)?)\Z/ and cmds[1].match /\A\d+x\d+\Z/
      options[:resolution] = cmds[1].to_s

    elsif cmds[0].match /\A(t(itle)?)\Z/
      options[:title] = cmds[1].to_s

    elsif cmds[0].match /\A(x(max)?)\Z/
      options[:xmax] = cmds[1].to_i
    end
  end

  options[:methods] = ["rectangle", "trapeze", "simpson", "gauss"]
end
