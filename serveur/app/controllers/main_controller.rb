class MainController < ApplicationController
  def index
    require_relative '../../../110borwein.rb'
    if params[:n]
      @out = main([params[:n].to_i.abs.to_s, "--resolution", "800x600", "--nodisplay"])
      @n = @out[0]
    end
  end
end
